//
//  SearchGithubUsersTests.swift
//  SearchGithubUsersTests
//
//  Created by Angelina Latash on 4/27/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import Nimble
import Quick
@testable import SearchGithubUsers

class SearchUserViewModelSpec: QuickSpec {
    
    override func spec() {
        describe("search(for userName:, completion:)") {
            context("search matches") {
                it("succeed") {
                    let sut = SearchUserViewModelMock()
                    let testUser = UserListModel(id: 10, avatarURL: "testUrlString", login: "searchTesting", profileURL: "testProfileURLString")
                    sut.users = [testUser]
                    let searchTest = "searchTest"
                    waitUntil { done in
                        sut.search(for: searchTest) { users in
                            expect(sut.users.count) > 0
                            expect(sut.users[0].login).to(contain(searchTest))
                            done()
                        }
                    }
                }
            }
            
            context("search doesn't match") {
                it("fails") {
                    let sut = SearchUserViewModelMock()
                    sut.users = []
                    let searchTest = "searchTest"
                    waitUntil { done in
                        sut.search(for: searchTest) { users in
                            expect(sut.users.count) == 0
                            done()
                        }
                    }
                }
            }
        }
        
        describe("selectUser(with userId:, completion:") {
            context("detail user model") {
                it("valid id") {
                    let sut = SearchUserViewModelMock()
                    let testId: Double = 1010
                    waitUntil { done in
                        sut.selectUser(with: testId) { detailUser in
                            expect(detailUser?.id).to(equal(testId))
                            done()
                        }
                    }
                }
                
                it("invalid id") {
                    let sut = SearchUserViewModelMock()
                    let testId: Double = 1011
                    waitUntil { done in
                        sut.selectUser(with: testId) { detailUser in
                            expect(detailUser?.id).toNot(equal(testId))
                            done()
                        }
                    }
                }
            }
        }
    }
}

class SearchUserViewModelMock: UsersInfoFetching {
    
    var users = [UserListModel]()
    
    func search(for userName: String, completion: @escaping SearchUserCompletion) {
        let userList = [UserListViewModel]()
        completion(userList)
    }
    
    func selectUser(with userId: Double, completion: @escaping (DetailUserModel?) -> Void) {
        let detailUser = DetailUserModel(id: 1010,
                                         login: "test user",
                                         avatarURL: "testUrlString",
                                         publicRepos: 1,
                                         followers: 3)
        completion(detailUser)
    }
}
