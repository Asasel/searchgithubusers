//
//  UserDetailInfoViewController.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/27/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class UserDetailInfoViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet private weak var avatar: UIImageView!
    
    @IBOutlet private weak var infoLabel: UILabel!
    
    private var viewModel: DetailUserModel!

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.login
        avatar.layer.cornerRadius = 8.0
        if let url = URL(string: viewModel.avatarURL) {
            avatar.sd_setImage(with: url)
        }
        infoLabel.text = """
        
        Public repositories: \(Int(viewModel.publicRepos))
        
        Followers: \(Int(viewModel.followers))
        """
    }
    
    deinit {
        Log.message("DEINIT: UserDetailInfoViewController")
    }
}

extension UserDetailInfoViewController {
    
    static func create(with detailUserModel: DetailUserModel) -> UserDetailInfoViewController {
        let controller = UserDetailInfoViewController.instantiateFromNib()
        controller.viewModel = detailUserModel
        return controller
    }
}
