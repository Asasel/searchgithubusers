//
//  SearchUserService.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

typealias UserListCompletion = ([UserListModel]) -> Void

typealias DetailUserCompletion = (DetailUserModel?) -> Void

class SearchUserService {

    // MARK: - Functions
    
    func findUsers(with name: String, completion: @escaping UserListCompletion) {
        guard let url = URL(string: "https://api.github.com/search/users?q=\(name)") else {
            return
        }
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data,
                let userModelRaw = try? JSONDecoder().decode(UserModelRaw.self, from: data) else {
                completion([])
                return
            }
            completion(userModelRaw.users)
        }
        task.resume()
    }
    
    func detailInfo(of profileURL: URL, completion: @escaping DetailUserCompletion) {
        let task = URLSession.shared.dataTask(with: profileURL) { data, _, _ in
            guard let data = data,
                let detailUserModel = try? JSONDecoder().decode(DetailUserModel.self, from: data) else {
                completion(nil)
                return
            }
            completion(detailUserModel)
        }
        task.resume()
    }
}
