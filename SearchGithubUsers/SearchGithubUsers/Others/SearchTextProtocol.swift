//
//  SearchTextProtocol.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/27/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

protocol SearchTextProtocol: class {

    var searchTextController: SearchTextController? { get set }

    var searchBarIsEmpty: Bool { get }

    var searchingText: String { get set }

    var requestSearchTimer: Timer? { get set }
    
    func configureSearchController()

    func reloadSearch()
}

extension SearchTextProtocol where Self: UIViewController {

    var searchBarIsEmpty: Bool {
        return searchTextController?.searchController.searchBar.text?.trimWhiteSpaces.isEmpty ?? true
    }

    func configureSearchController() {
        definesPresentationContext = true
        navigationItem.searchController = searchTextController?.searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        searchTextController?.updateSearchText = { [weak self] searchText in
            self?.searchingText = searchText
            self?.searchTimerAction()
        }
        searchTextController?.cancelSearch = { [weak self] in
            self?.searchTextController?.searchController.searchBar.text = ""
            self?.searchTextController?.searchController.isActive = false
            self?.reloadSearch()
        }
    }

    private func searchTimerAction() {
        requestSearchTimer?.invalidate()
        requestSearchTimer = Timer(timeInterval: 0.5, repeats: false) { [weak self] _ in
            self?.reloadSearch()
        }
        guard let requestSearchTimer = requestSearchTimer else {
            return
        }
        RunLoop.current.add(requestSearchTimer, forMode: RunLoop.Mode.common)
    }
}
