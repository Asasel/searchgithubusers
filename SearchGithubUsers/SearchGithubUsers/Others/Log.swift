//
//  Log.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

class Log {

    // MARK: - Constants

    static let showLog = true

    // MARK: - Construction

    static func message(_ message: String) {
        guard showLog else {
            return
        }
        print(message)
    }
}
