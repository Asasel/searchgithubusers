//
//  SearchTextController.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/24/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

typealias LightweightClosure = () -> Void

class SearchTextController: NSObject {

    // MARK: - Properties

    private(set) var searchController: UISearchController

    private(set) var requestSearchTimer: Timer?

    var updateSearchText: ((String) -> Void)?

    var cancelSearch: LightweightClosure?

    // MARK: - Construction

    init(placeholderText: String = "Search") {
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = placeholderText
        super.init()
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.returnKeyType = .done
    }

    // MARK: - Functions

    deinit {
        Log.message("DEINIT: SearchTextController")
    }
}

extension SearchTextController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        updateSearchText?(searchController.searchBar.text ?? "")
    }
}

extension SearchTextController: UISearchBarDelegate {

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearch?()
    }
}
