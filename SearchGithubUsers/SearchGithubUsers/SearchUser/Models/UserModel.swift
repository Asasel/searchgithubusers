//
//  UserModel.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct UserModelRaw: Decodable {
    
    let users: [UserListModel]
    
    enum CodingKeys: String, CodingKey {
        case
        users = "items"
    }
}

struct UserListModel: Decodable {
    
    let id: Double
    
    let avatarURL: String

    let login: String
    
    let profileURL: String
    
    enum CodingKeys: String, CodingKey {
        case
        id,
        avatarURL = "avatar_url",
        login,
        profileURL = "url"
    }
}
