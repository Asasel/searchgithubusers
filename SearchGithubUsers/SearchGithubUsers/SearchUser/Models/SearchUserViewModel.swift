//
//  SearchUserViewModel.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

typealias SearchUserCompletion = ([UserListViewModel]) -> Void

protocol UsersInfoFetching {
    
    var users: [UserListModel] { get set }
    
    func search(for userName: String, completion: @escaping SearchUserCompletion)
    
    func selectUser(with userId: Double, completion: @escaping (DetailUserModel?) -> Void)
}

class SearchUserViewModel: UsersInfoFetching {
    
    // MARK: - Properties
    
    private var searchUserService = SearchUserService()
    
    var users = [UserListModel]()
    
    // MARK: - Functions
    
    func search(for userName: String, completion: @escaping SearchUserCompletion) {
        searchUserService.findUsers(with: userName) { [weak self] users in
            self?.users = users
            let viewModels = users.compactMap { UserListViewModel(id: $0.id, name: $0.login, imageURL: $0.avatarURL) }
            completion(viewModels)
        }
    }
    
    func selectUser(with userId: Double, completion: @escaping (DetailUserModel?) -> Void) {
        let selectedUser = users.first(where: { $0.id == userId })
        guard let profileURL = selectedUser?.profileURL,
            let url = URL(string: profileURL) else {
            return
        }
        searchUserService.detailInfo(of: url) { detailUserInfo in
            DispatchQueue.main.async { 
                completion(detailUserInfo)
            }
        }
    }
    
    func presentDetailInfoController(for detailUserModel: DetailUserModel,
                                     on navigationController: UINavigationController?) {
        let detailViewController = UserDetailInfoViewController.create(with: detailUserModel)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}
