//
//  DetailUserModel.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/27/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct DetailUserModel: Decodable {
    
    let id: Double
    
    let login: String
    
    let avatarURL: String
    
    let publicRepos: Double
    
    let followers: Double
    
    enum CodingKeys: String, CodingKey {
        case
        id,
        login,
        avatarURL = "avatar_url",
        publicRepos = "public_repos",
        followers
    }
}
