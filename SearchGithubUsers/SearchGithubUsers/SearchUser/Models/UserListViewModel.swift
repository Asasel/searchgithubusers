//
//  UserListViewModel.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

struct UserListViewModel {
    
    let id: Double
    
    let name: String
    
    let imageURL: String
    
    var url: URL? {
        return URL(string: imageURL)
    }
}
