//
//  SearchUserTableViewCell.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation
import UIKit

class SearchUserTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var avatar: UIImageView!
    
    // MARK: - View Life cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatar.layer.cornerRadius = 8.0
    }
}
