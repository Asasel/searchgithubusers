//
//  SearchUserViewController.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/23/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import UIKit
import SDWebImage

class SearchUserViewController: UIViewController {
    
    // MARK: - Constants
    
    let cellIdentifier = "SearchUserTableViewCell"

    // MARK: - Properties
    
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var notFoundLabel: UILabel!
    
    private var viewModel = SearchUserViewModel()
    
    var cellViewModels = [UserListViewModel]()
    
    var searchTextController: SearchTextController?

    var requestSearchTimer: Timer?

    var searchingText = ""

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search GitHub Users"
        searchTextController = SearchTextController()
        configureSearchController()
        configureTableView()
    }

    // MARK: - Private functions
    
    private func configureTableView() {
        let cellNib = UINib(nibName: cellIdentifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
}

extension SearchUserViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SearchUserTableViewCell
        let viewModel = cellViewModels[indexPath.row]
        cell.name.text = viewModel.name
        cell.avatar.sd_setImage(with: viewModel.url)
        return cell
    }
}

extension SearchUserViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userId = cellViewModels[indexPath.row].id
        viewModel.selectUser(with: userId) { [weak self] detailInfo in
            guard let self = self,
                let detailInfo = detailInfo else {
                return
            }
            self.viewModel.presentDetailInfoController(for: detailInfo, on: self.navigationController)
        }
    }
}

extension SearchUserViewController: SearchTextProtocol {
    
    func reloadSearch() {
        if searchingText.trimWhiteSpaces.isEmpty {
            notFoundLabel.isHidden = true
            cellViewModels.removeAll()
            tableView.reloadData()
            return
        }
        viewModel.search(for: searchingText) { [weak self] users in
            self?.cellViewModels = users
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                self.notFoundLabel.isHidden = !self.cellViewModels.isEmpty
                self.tableView.reloadData()
            }
        }
    }
}

extension SearchUserViewController {
    
    static func create() -> SearchUserViewController {
        return SearchUserViewController.instantiateFromNib()
    }
}

