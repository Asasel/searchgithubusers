//
//  StringExtension.swift
//  SearchGithubUsers
//
//  Created by Angelina Latash on 4/27/20.
//  Copyright © 2020 Angelina Latash. All rights reserved.
//

import Foundation

extension String {
    
    var trimWhiteSpaces: String {
        return replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "\t", with: "")
    }
}
