# README #

### Contribution guidelines ###

The app "Search Github Users" enables the user to search for Github users and displays the results of that search in a list. Each result item displays:

• The user image
• The user name

When we click a detail item. It will display a detail view:

• Number of public repositories
• Number of followers
